﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class Destructor : MonoBehaviour {

	// how much dmg this destructor does
	public double damageAmount = 2;

	// the real dmg of this destructor (needed if pooling)
	private double realDamage = 0;

	void Awake()
	{
		// get value from inspector and save this as the "real damage" before pooling
		realDamage = damageAmount;
	}

	// reset state of pooled destructor
	void OnEnable()
	{
		damageAmount = realDamage;
	}


    public virtual void DoDamage(Destructible dest)
    {
        dest.TakeDamage(damageAmount);


    }

	// called when the source of dmg is a player
	public virtual void DoPlayerDamage(Destructible dest)
	{
		dest.TakePlayerDamage(damageAmount);


	}

    // used by projectiles to add dmg
	public void ModifyDamage(int dmg)
	{
		damageAmount += dmg;

		if(damageAmount < 1)
		{
			// sanity check - if subtracting damage can't go below 1
			damageAmount = 1;
		}
	}

	public void SetDamage(int dmg)
	{
		damageAmount = dmg;
	}
		
}
