﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjTimedSpawner : MonoBehaviour {

	// what are we spawning
	public Rigidbody2D objToSpawn;

	// how often we spawn
	public float spawnInterval = 3.0f;

	// how many max
	public int spawnLimit = 10;

	// keep track of what and how many we spawn
	private List<GameObject> spawnList;

	// last time we spawned something
	private float lastTimeSpawned = 0;


	void Awake () {
		spawnList = new List<GameObject>();
	}

	void Update () {

		// only spawn if the interval has passed and we haven't exceeded the spawn limit count
		if(Time.time - lastTimeSpawned > spawnInterval && spawnList.Count < spawnLimit)
		{

			// spawn the prefab enemy ship
			Rigidbody2D spawnedObject = Instantiate<Rigidbody2D>( objToSpawn );

			// set it's start position to be the attached game obj's start position
			spawnedObject.transform.position = this.gameObject.transform.position;

			// set parent to this
			spawnedObject.GetComponent<Destructible>().parentSpawner = this;

			// add this to the spawnList (gameobject itself)
			spawnList.Add(spawnedObject.gameObject);

			// used to track spawn interval
			lastTimeSpawned = Time.time;
		}



	}

	// remove from spawnlist when a child dies
	public void notifyOfDeath(GameObject dyingObj)
	{
		spawnList.Remove(dyingObj);
	}
}
