﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {

	public double maximumHitPoints = 10;

	public float invincibilityTime = 0.0f;

	protected float lastTimeHurt;


	// reference to parent spawner if applicable
	public ObjTimedSpawner parentSpawner = null;


	// floating text msg to show when ship is destroyed
	public bool floatingTextWhenDestroyed = false;

	// amount of damage done by the player (vs other sources)
	public double playerDamageDone = 0;



	// cached vars for credits given calc
	//private float creditsGiven;
	//private int creditsInt;

	public GameObject deathAnimObj;

    // audio
    public AudioSource hurtNoise;

	// current hit points
	private double hitPoints;

	// are we dying
	private bool isDying;

	// Use this for initialization
	void Start () {
		hitPoints = maximumHitPoints;

		// made sure we passed invincibility time before we get hurt again
		lastTimeHurt = Time.time - invincibilityTime;

		// get animator component if available
		// animator = GetComponent<Animator>();
	}

    public void PlayHurtNoise()
    {
        if (hurtNoise != null)
        {
            hurtNoise.Play();
        }
    }

	public virtual void TakeDamage( double amount )
	{

        // play a noise when you take damage
        PlayHurtNoise();

		// assumes damage is always positive value
		ModifyHitPoints( -amount );

		// log last time hurt as the present time
		lastTimeHurt = Time.time;



	}

	// called when the player damages a destructible
	public virtual void TakePlayerDamage( double amount)
	{
		// same as take damage but keep track of how much dmg the player has done vs others
		playerDamageDone += amount;

		TakeDamage(amount);

		//Debug.Log("PLAYER DONE:" + playerDamageDone);
	}

	public virtual void RecoverHitPoints( double amount )
	{
		ModifyHitPoints( amount );

	}



	public virtual void ModifyHitPoints( double amount )
	{
		// return if we were hurt too soon - also amonut < 0 makes sure this doesn't trigger on heals
		if( Time.time - lastTimeHurt < invincibilityTime && amount < 0)
		{ return; }

		hitPoints += amount;

		// if we're healing don't go over the max
		hitPoints = (double)Mathf.Min((int)hitPoints, (int)maximumHitPoints);

		//Debug.Log("hp:" + hitPoints);

		if( hitPoints <= 0)
		{
			Die();
		}

	}


	public virtual void Die()
	{
		if( isDying )
		{
		    // we already died - don't do it again
		    return;
		}

		isDying = true;

		if(parentSpawner != null)
		{
			// notify parent spawner of death if one exists for this ship
			parentSpawner.notifyOfDeath(this.gameObject);
		}

		// draw an explosion animation where this ship was
		if(deathAnimObj != null)
		{
			GameObject explosionObject =  Instantiate(deathAnimObj, transform.position, Quaternion.identity);

			// scale explosion size appropraitely
			explosionObject.transform.localScale = this.transform.localScale;
		}

		// destroy this game object
		Object.Destroy(this.gameObject);


	}
		
	

}
