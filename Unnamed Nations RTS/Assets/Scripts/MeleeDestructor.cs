﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeDestructor : Destructor {

	// cooldown per hit
	public float atkCooldown = 0.5f;

	// which destructible got hit
	private Destructible whatGotHit;

	public void OnTriggerStay2D(Collider2D collision)
	{
		//Debug.Log("COL: " + gameObject.name + " and " + collision.gameObject.name);

		// grab the destructible component from the object that hit us
		whatGotHit = collision.GetComponent<Destructible>();

		if(whatGotHit != null)
		{
			DoDamage(whatGotHit);
		}


	}
}
