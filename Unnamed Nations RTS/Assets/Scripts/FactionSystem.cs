﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionSystem : MonoBehaviour {

	// TODO Change this to Enum for code cleanup
	// list of the faction Tags
	public const string PLAYER = "Player";
	public const string ENEMY = "Enemy";
	public const string PIRATE = "Pirate";
	public const string FACTION4 = "Faction4";
	public const string FACTION5 = "Faction5";

	// faction 2d array rows go top to bottom
	//
	// PLAYER
	// ENEMY
	// PIRATE
	// FACTION4
	// FACTION5
	// same order left to right
	//
	// new factions are a new row on the bottom and a new col on the rightmost side

	// the array indices that go with each faction
	private const int PLAYER_IDX = 0;
	private const int ENEMY_IDX = 1;
	private const int PIRATE_IDX = 2;
	private const int FACTION4_IDX = 3;
	private const int FACTION5_IDX = 4;

	private int numOfFactions = 5;
//	private int rows;
//	private int cols;

	public bool[,] alliances;

	void Awake()
	{
		// init array structure
//		rows = numOfFactions;
//		cols = numOfFactions;

		alliances = new bool[numOfFactions, numOfFactions];

		// set initial alliances
		// all to false
		for(int i = 0; i < numOfFactions; i++)
		{
			for(int j = 0; j < numOfFactions; j++)
			{
				alliances[i,j] = false;
			}
		}

		// now ally all to themselves (so our weapons don't shoot ourselves)
		for(int i = 0; i < numOfFactions; i++)
		{
			// so we don't shoot ourselves
			alliances[i,i] = true;
		}



	}

	// Use this for initialization
//	void Start () {
//		
//		for(int i = 0; i < numOfFactions; i++)
//		{
//			for(int j = 0; j < numOfFactions; j++)
//			{
//				//Debug.Log("ally state of " + i + "," + j + " is:" + alliances[i,j]);
//
//			}
//		}
//	}

	// returns true if the 2 passed in factions are allied, else false
	// pass in the Faction System faction tag string constants or gameobject tags
	public bool CheckAlliance(string fac1, string fac2)
	{
		// check for the default tag
		if(fac1 == "Untagged"  || fac2 == "Untagged") {return false;}

		return alliances[ GetAllianceIdx(fac1),GetAllianceIdx(fac2) ];
	}

	public void CreateAlliance(string fac1, string fac2)
	{
		// ally both combinations in the array as they appear twice
		alliances[ GetAllianceIdx(fac1),GetAllianceIdx(fac2) ] = true;
		alliances[ GetAllianceIdx(fac2),GetAllianceIdx(fac1) ] = true;
	}

	public void BreakAlliance(string fac1, string fac2)
	{
		// break both combinations in the array as they appear twice
		alliances[ GetAllianceIdx(fac1),GetAllianceIdx(fac2) ] = false;
		alliances[ GetAllianceIdx(fac2),GetAllianceIdx(fac1) ] = false;
	}

	private int GetAllianceIdx(string facStr)
	{
		switch(facStr)
		{
		case PLAYER: return PLAYER_IDX;
		case ENEMY: return ENEMY_IDX;
		case PIRATE: return PIRATE_IDX;
		case FACTION4: return FACTION4_IDX;
		case FACTION5: return FACTION5_IDX;
		}
	
		// error - just assume it's enemy
		return ENEMY_IDX;
	}
}
