﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : MonoBehaviour {

	// wanders randomly from origin and then when we hit wander radius edge or max wander time go back to origin and repeat 

	// origin position of wandering
	public Vector3 originPosition;

	// move speed of this walker
	public float speed;

	// how far to wander from origin position
	public float wanderRadius = 1000f;

	// how long before changing direction
	public float wanderTimeMin = 5.0f;
	public float wanderTimeMax = 15.0f;
	private float wanderTime;
	private float timeElapsed = 0.0f;

	// have we reached the wander destination point yet
	private bool newPositionChosen = false;

	// go back to origin now?
	private bool backToOrigin = false;

	// cached variables
	private Rigidbody2D rb;
	private Vector3 randomPos;
	private Vector2 direction;


	void Awake()
	{
		// initialize with some sort of value
		randomPos = new Vector3(0,0,0);
	}

	void Start()
	{
		// where we are first created becomes our origin position
		originPosition = this.gameObject.transform.position;

		// get a reference to the rigidbody
		rb = GetComponent<Rigidbody2D>();

	}

	void Update() 
	{

		// pick a new location if one hasn't been chosen.
		// if it has been chosen just move to that location
		if(newPositionChosen == false)
		{
			// reset time elapsed
			timeElapsed = 0.0f;

			if(backToOrigin == true)
			{
				// called if attempting to go back to origin after wandering too far

				// set our wander point back to the origin
				randomPos = originPosition;

				// reset back to origin state
				backToOrigin = false;
			}
			else
			{
				// pick a random point within wander radius
				randomPos.x = originPosition.x + Random.Range(-wanderRadius, wanderRadius);
				randomPos.y = originPosition.y + Random.Range(-wanderRadius, wanderRadius);
			}

			// pick a random wandering time
			wanderTime = Random.Range(wanderTimeMin, wanderTimeMax);

			// flag that we chose a new position
			newPositionChosen = true;

			//Debug.Log("NEW POSITION PICKED: " + gameObject.name + "  is  " + randomPos);
		}
		else
		{
			// we have a destination

			// move to that point
			direction.x = randomPos.x - transform.position.x;
			direction.y = randomPos.y - transform.position.y;
			direction = direction.normalized;
			rb.velocity = direction * speed * Time.deltaTime;

			// add time elapsed
			timeElapsed += Time.deltaTime;

			// if we reached our destination or wander time is up pick a new destination
			if( (Vector2.Distance(transform.position, randomPos) <= 0.1f ) || (timeElapsed > wanderTime) )
			{
				//Debug.Log(gameObject.name + ":  here: " + transform.position + " goal: " + randomPos);

				// reset by choosing a new wander position
				newPositionChosen = false;

			}

			// if we're beyond wander radius go back to center
			if(Vector2.Distance(transform.position, originPosition) > wanderRadius)
			{
				newPositionChosen = false;
				backToOrigin = true;
			}

		}



	}
}
